#!/bin/sh
# Miloš Komarčević <kmilos@gmail.com>, 2012.

# Alternativno dodati proveru da li 'serbia.osm.bz2' uopšte postoji,
# kao i da već nije možda nije raspakovana

#wget -N http://download.geofabrik.de/osm/europe/serbia.osm.bz2
#bunzip2 -k -f -q serbia.osm.bz2

POLIGONI=`ls Regioni_poly/RS*.poly`

counter=0
# Glavna petlja, pretpostavlje da je 'osmosis' u PATH
for i in ${POLIGONI}; do
    o=Regioni/`basename $i .poly`.osm
    komanda=$komanda" --bounding-polygon file=$i --write-xml file=$o"
    counter=`expr $counter + 1`
done

osmosis --read-pbf file=serbia-latest.osm.pbf --tf reject-relations --tee $counter $komanda

#rm serbia.osm
