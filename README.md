# Скрипте којима се аутоматизује генерисање фајлова за пресловљавање.


Рендер сервер се инсталира према [https://switch2osm.org/manually-building-a-tile-server-18-04-lts/](https://switch2osm.org/manually-building-a-tile-server-18-04-lts/)
    

Да бисмо увезли `claimed_administrative` границу (граница Србије са Косовом), треба да печујемо openstreetmap-carto, то може да се уради са командом:
```
cd ~/src/openstreetmap-carto/
cat ~/OSM_skripte/add_claimed_administrative_to_roads.diff | patch -u
```

Подразумева се да је инсталиран osmosis и osmfilter 
```
apt install osmctools оsmosis
git clone https://gitlab.com/mpele/OSM_skripte.git
git clone https://github.com/mpele/JOSM_transliterator.git # - u direktorijum /home/renderaccount/
```

Ne zaboraviti da se instalira php
```
sudo apt-get install php libapache2-mod-php
```

Ako se želi prerendering tile-ova, treba instalirati:
```
cd src
git clone https://github.com/alx77/render_list_geo.pl.git
```

Crontab za korisnika osm
`0 6 * * * /home/renderaccount/OSM_skripte/07_obradi_podatke_za_render > /home/osm/izlaz.txt 2>&1`

Generisanje kesa
  na lokalnom racunaru kreirati direktorimum 
  pozeljno je da bude XFS
```
sudo mkdir /var/www/html/kes
sudo chmod 777 /var/www/html/kes    # podesiti odgovarajuce privilegije
```

 
# Latinica
renderd.conf iz pomocniZaRender iskopirati u /usr/local/etc/renderd.conf

project_lat.mml treba da se napravi u odnosu na verziju openstreetmap-carto verzije.
Trenutno postoji patch za verziju v4.21.1. Primeniti ga sa:
```
cd ~/src/openstreetmap-carto/
cat ~/OSM_skripte/project_lat.v4.21.1.diff | patch -u
```

Ako nemate tu verziju, takođe postoji i fajl `project_lat.mml` koji je stariji i možda ne radi.
project_lat.mml iz pomocniZaRender iskopirati u ~/src/openstreetmap-carto/.

Posle toga, pokrenuti:
```
carto project_lat.mml > mapnik_lat.xml
```

U principu, za render latinice u project_lat.mml prepravalja se:
```sql
            COALESCE(tags->'name:sr-Latn', name) as name
```
ili nesto tipa:
```sql
            CASE WHEN (tags->'name:sr-Latn' is not null)
              THEN tags->'name:sr-Latn' || ' ('  || name ||  ')'
              ELSE '*' || name END AS name,
```

ali radnja je pipava i bolje koristiti ponuđeni patch ili gotov fajl.


# Српски курзив 
```
sudo cp ~/OSM_skripte/pomocniZaRender/NotoSansSrpski-Italic.ttf /usr/share/fonts/truetype/noto/NotoSansSrpski-Italic.ttf
fc-cache -fv
sudo fc-cache -fv
```

u style/fonts.mml referencirati italik font kao "Noto Sans Srpski Italic".

```
carto project.mml > mapnik.xml
carto project_lat.mml > mapnik_lat.xml
```


# КОНФИГУРАЦИЈА

Конфигурација скипте се подразумевано налази у priprema_rendera_config.sh. Та датотека не постоји на GIT већ треба прекопирати priprema_rendera_config_primer.sh у priprema_rendera_config.sh. 

У priprema_rendera_config.sh се могу подешавати параметри прилагођени локалном окружењу. Ову датотеку не треба стављати на GIT јер свако код себе држи различиту конфигурацију.

Све опције за конфигурацију су документоване у priprema_rendera_config_primer.sh.

Ако је потребно да се користи више различитих конфигурација (ради тестирања на пример) може се направити више конфигурационх датотека тако што ће се priprema_rendera_config_primer.sh ископирати у нову датотеку у којој ће се део "primer" заменити неком другом речју. Та реч се може поставити као први параметар приликом покретања priprema_rendera.sh и тада ће скрипт учитати конфигурацију која садржи наведену реч. 

На пример, да би се учитала конфигурација priprema_rendera_config_test.sh треба покренути 

```
. priprema_rendera.sh test

```

# Приказ мапе

Линковати фајлове за приказ 
```
sudo ln -s /home/renderaccount/OSM_skripte/phpSkripteZaSajt/index.html /var/www/html/
sudo ln -s /home/renderaccount/OSM_skripte/phpSkripteZaSajt/i.html /var/www/html/
sudo ln -s /home/renderaccount/OSM_skripte/phpSkripteZaSajt/kombinovano.html /var/www/html/
sudo ln -s /home/renderaccount/OSM_skripte/phpSkripteZaSajt/tiles.php /var/www/html/
sudo ln -s /home/renderaccount/OSM_skripte/phpSkripteZaSajt/kes.html /var/www/html/
sudo ln -s /home/renderaccount/OSM_skripte/phpSkripteZaSajt/kesTiles.php /var/www/html/
```

 
Линковати директоријум зарад приказа фајлова на веб серверу:
`sudo ln -s /home/renderaccount/OSM_skripte/zaSajt/ /var/www/html/preslovljavanje`

  iskopirati na udaljeni racunar skriptu uploadKes.php 
  ili za testiranje u lokalu 
`sudo ln -s /home/renderaccount/OSM_skripte/phpSkripteZaSajt/uploadKes.php /var/www/html/`
 

