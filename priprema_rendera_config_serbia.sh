#!/bin/sh


# ovde se podesava sta sve treba da se pripremi za rendering

PROCESS_SERBIA_BORDER_DB=true
PROCESS_SERBIA=true
PROCESS_KIM=true

PROCESS_BOSNIA_HERZEGOVINA=true
PROCESS_CROATIA=true
PROCESS_MONTENEGRO=true

PROCESS_ALBANIA=true
PROCESS_BULGARIA=true
PROCESS_GREECE=true
PROCESS_HUNGARY=true
PROCESS_MACEDONIA=true
PROCESS_ROMANIA=true

PROCESS_AUSTRIA=false
PROCESS_ITALY=false


# da li se podaci prepisuju inkrementalno
#   true - kakose koja zemlja obradi tak se prepisuje
#   false - prvo se obrade sve zemlje pa se odjednom sve spoje 
INCREMENTAL_PROCESS=false

# nacin na koji se spajaju podaci
#   file - sve se prepisuje u jedan pbf i onda ucitava u bazu
#   database - svaka pojedinacna datoteka se ucitava u bazu
MERGE_TYPE="file"
#MERGE_TYPE="database"



# da li se pripremljena objedinjena mapa ucitava u bazu za rendering
SEED_DATABASE=true


# sabloni pravila preslovljavanja 

# preslovljavanje cirilice za zemlje sa osnovnim cirilicnim pismom
TSLT_CIR_CIR="name:sr@name@name:sr-Latn@name:en"

# preslovljavanje latinice za zemlje sa osnovnim cirilicnim pismom
TSLT_CIR_LAT="name:sr-Latn@Pname:sr@name@Pname:en@name"

# Kosovo i Metohija ima posebna pravila
TSLT_KIM_CIR="name:sr@name:sr-Latn@name:en@name"
TSLT_KIM_LAT="name:sr-Latn@Pname:sr@name:en@name"

# preslovljavanje latinice za Srbiju
TSLT_SRB_LAT="name:sr-Latn@Pname:sr@name@name:en@name"

# preslovlajvanje cirilice za zemlje sa osnovnim latinicnim pismom
TSLT_LAT_CIR="name:sr@name:sr-Latn@name@name:en"

# preslovljavanje latinice za zemlje sa osnovnim latinicnim pismom
TSLT_LAT_LAT="name:sr-Latn@Pname:sr@name@Pname:en@name"
TSLT_LAT_LAT="name:sr-Latn@Pname:sr@name@Pname:en@name"
                          


                          
set JAVACMD_OPTIONS=-server -Xmx2600m

memZaOsm2pgsql=600


# Ako se koriste lokalni podaci skripta ce uzimati ranije sacuvane podatke
# Ako nema sacuvanih podataka automatski ce ih preuzeti sa geofabrik
# na produkcionom serveru treba da bude false da bi svaki put preuzimao update
USE_LOCAL_DATA=true

# Direktorijum gde se cuvaju lokalno sacuvani podaci
LOCAL_DATA_DIR="/data/priprema_rendera/local_files"

# Direktorijum gde treba smestati privremeno generisane podatke
TMP_DIR="/data/priprema_rendera/tmp"

# Granice podrucja za koje se radi rendering.
# Podaci u bazi ce biti ograniceni samo na ovu oblast
# preuzeti iz tileserver/bounding-box.txt
# exYU
#BOUNDING_BOX="top=47.2 left=11.0 bottom=40.8 right=25.3"
# Srbija
BOUNDING_BOX="top=47.09 left=16.83 bottom=40.93 right=25.36"

#
PRERENDER_TILES=true
CLEAR_TILE_CACHE=true
MAX_ZOOM_TO_PRERENDER=10
PRERENDER_BOUNDING_BOX="-x 18.7 -X 23 -y 42 -Y 46.3"
RENDERING_THREADS=1
RENDERING_SYSTEM_LOAD=16


# putanje gde se nalaze kesirane slicice 
TILESERVER_CACHE="/var/www/html/tiles/osmserbia/"
RENDER_CACHE_CIR="/data/mod_tile/ajt/"
RENDER_CACHE_LAT="/data/mod_tile/lat/"
