#!/bin/sh
set -e
set -x

date
wget  http://download.geofabrik.de/europe/serbia-latest.osm.pbf
date

echo ------ Raspakuje fajl u serbia.osm ------ 
#osmosis --rb serbia-latest.osm.pbf --wx serbia.osm
#date

echo  ------ Izdvaja relacije ------ 
## izdvaja fajl sa relacijama
osmosis --rb file=serbia-latest.osm.pbf --tf reject-nodes --tf reject-ways --wx osm/xRelacije.osm 
date

echo  ------ deli fajl srbija.osm na okruge i odbacuje sve relacije ------ 
## deli fajl srbija.osm na okruge i odbacuje sve relacije
./01_podeli_na_okruge.sh 
date

echo  ------ procesira okruge ------ 
## izdvaja gde treba praviti relacije
./02_procesiraj_okruge_preslovljavanje.sh

echo  ------ pregled naziva ------ 
## pregled naziva
./03_napravi_preglede_preslovljavanje.sh

#./04_napravi_preglede_pumpe.sh


##./05_za_osmand.sh



#zip arhiva le*.osm RS*.html
#rm *.osc
#rm *.osm
#rm *.html
rm serbia-latest.osm.pbf*
rm Regioni/*.osm
date

date
echo Gotovo 
