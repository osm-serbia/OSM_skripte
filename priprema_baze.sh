#!/bin/sh
set -e
#set -x

echo

# Radni direktorijum skripte
#HOME_DIR="$( cd "$(dirname "$0")" ; pwd -P )"
cd $HOME_DIR

#CONFIG_FILE="$HOME_DIR/priprema_rendera_config.sh"

#ucitava podesavanja
#if [ -f "$CONFIG_FILE" ]
#then
#  . $CONFIG_FILE
#else
#  echo
#  echo 'Nije nadjena konfiguracija: '$CONFIG_FILE
#  echo 'Prekopirajte priprema_rendera_config_primer.sh u priprema_rendera_config.sh'
#  echo
#  exit
#fi


#ucitava biblioteku
. $HOME_DIR/priprema_rendera_lib.sh

echo ''
echo ''
echo 'Priprema baze za render'
echo ''

m_database_start_timer=$(date)
echo 'Zapoceto: '$m_database_start_timer
echo ''




#if [ ! -f "$INPUT_FILE" ]; then
#  echo "Nije navedena datoteka. Ucitavam: "$BASE_FILE
#  INPUT_FILE="$BASE_FILE"
#  if [ ! -f "$INPUT_FILE" ]; then
#    echo "Ne postoji: "$BASE_FILE
#    echo
#    exit
#  fi
#fi

#echo "SEED_DATABASE: $SEED_DATABASE" 

# ucitava u bazu
if $SEED_DATABASE;
then

  # initialize output file
  BASE_FILE="$TMP_DIR"/base_file.pbf
  #INPUT_FILE="$TMP_DIR"/base_file.pbf

  if [ "$INCREMENTAL_PROCESS" = false ]; then  
    if [ ! -z "$file_list" ]; then
      INPUT_LIST=$file_list
    else
      echo 
      echo "Nema pripremljenih datoteka za prepisivanje"
      echo
      exit
    fi
  else
    INPUT_FILE="$1"
    if [ -f "$INPUT_FILE" ]; then
      INPUT_LIST=$INPUT_FILE
    else
      INPUT_LIST=$BASE_FILE
    fi  
  
  fi

  seed_database $INPUT_LIST
  #rm $INPUT_FILE
fi

# menja granice kroz bazu
if $PROCESS_SERBIA_BORDER_DB; then

  if [ $PROCESS_SERBIA == true ] || [ $PROCESS_KIM = true ];  then 

    echo
    echo ----------
    echo $(date)
    echo "Postavljanje granica"
    echo

    # prvo pokupimo id-ove svih granica koje nam trebaju
    kosovo_id=`psql -X -A -d gis -t -c "select distinct osm_id from planet_osm_roads where admin_level='2' and boundary='administrative' and tags -> 'name:sr' = 'Косово и Метохија' limit 1"`
    serbia_id=`psql -X -A -d gis -t -c "select distinct osm_id from planet_osm_roads where admin_level='2' and boundary='administrative' and tags -> 'name:sr' = 'Србија' limit 1"`
    serbia_claimed_id=`psql -X -A -d gis -t -c "select distinct osm_id from planet_osm_roads where boundary='claimed_administrative' and tags -> 'name:sr' = 'Србија' and tags -> 'claimed:admin_level' = '2' limit 1"`

    echo "Nađena granica Kosova je $kosovo_id, granica Srbije je $serbia_id, a claimed granica je $serbia_claimed_id"

    if [ -z "$kosovo_id" ] || [ -z "$serbia_id" ] || [ -z "$serbia_claimed_id" ];
    then
      echo "Neka granica nije nađena, procesiranje staje!!!"
      exit 1
    fi

    # drop admin levels from Kosovo
    psql -X -A -d gis -t -c "delete from planet_osm_roads where osm_id<0 and (admin_level='6' or admin_level='4') and boundary='administrative' and way@(select way from planet_osm_polygon where osm_id=$kosovo_id)"
    # change kosovo border
    psql -X -A -d gis -t -c "update planet_osm_roads set admin_level=4 where osm_id=$kosovo_id"
    psql -X -A -d gis -t -c "update planet_osm_polygon set admin_level=4 where osm_id=$kosovo_id"
    # rename Kosovo
    psql -X -A -d gis -t -c "update planet_osm_roads set name='Косово и Метохија', tags = tags || '\"name:sr-Latn\"=>\"Kosovo i Metohija\" ' where osm_id=$kosovo_id"
    psql -X -A -d gis -t -c "update planet_osm_polygon set name='Косово и Метохија', tags = tags || '\"name:sr-Latn\"=>\"Kosovo i Metohija\" ' where osm_id=$kosovo_id"
    # delete Serbia border
    psql -X -A -d gis -t -c "delete from planet_osm_roads where osm_id=$serbia_id"
    # set claimed border as real border
    psql -X -A -d gis -t -c "update planet_osm_roads set admin_level=2, boundary='administrative' where osm_id=$serbia_claimed_id"
  fi
fi # $PROCESS_SERBIA_BORDER_DB;


## pokreni prerender tileova
##if $SEED_DATABASE && $PRERENDER_TILES;
#if $PRERENDER_TILES;
#then
#
#  . $HOME_DIR/priprema_rendera_prerender.sh
#fi  


echo ''
echo 'Priprema baze zapoceta: '$m_database_start_timer

m_database_end_timer=$(date)
echo 'Priprema baze:' $m_database_end_timer

D1=$(date -d "$m_database_start_timer" '+%s'); 
D2=$(date -d "$m_database_end_timer" '+%s')
echo "Priprema baze trajala: $(((D2-D1)/86400))d $(date -u -d@$((D2-D1)) +%H:%M)"
echo ''
echo ''
echo ''

echo
