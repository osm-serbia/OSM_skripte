#!/bin/sh
set -e
#set -x
#
# Prema uputstvu
# https://wiki.openstreetmap.org/wiki/HowTo_minutely_hstore
#
#

echo

# Radni direktorijum skripte
HOME_DIR="$( cd "$(dirname "$0")" ; pwd -P )"
cd $HOME_DIR

DEFAULT_CONFIG_FILE="$HOME_DIR/priprema_rendera_config.sh"

config_id=$1

if [ -z "$config_id" ]; then
  CONFIG_FILE="$DEFAULT_CONFIG_FILE"
else
  CONFIG_FILE="$HOME_DIR/priprema_rendera_config_$config_id.sh"
fi

#inicijalizacija
INIT_FILE="$HOME_DIR/priprema_rendera_init.sh"
. $INIT_FILE


#ucitava podesavanja
if [ -f "$CONFIG_FILE" ]
then
  echo 'Konfiguracija: '$CONFIG_FILE
  . $CONFIG_FILE
else
  echo
  echo 'Nije nadjena konfiguracija: '$CONFIG_FILE
  echo 'Prekopirajte priprema_rendera_config_primer.sh u priprema_rendera_config.sh'
  echo
  exit
fi

#ucitava podesavanja
#. $HOME_DIR/priprema_rendera_config.sh


#ucitava biblioteku
. $HOME_DIR/priprema_rendera_lib.sh

# podesavanje sta se obradjuje u priprema_rendera je prebaceno u config

echo ''
echo ''
echo ''
echo "====================================================="
echo 'Update podataka za render'
echo "-----------------------------------------------------"
echo ''

m_start_timer=$(date)
echo 'Zapoceto: '$m_start_timer
echo ''



export WORKDIR_OSM=$DATA_DIR/update_rendera


# initialize download file
DOWNLOAD_FILE="$WORKDIR_OSM"/planet_changes_file.osc
if [ -f "$DOWNLOAD_FILE" ]; then
  rm $DOWNLOAD_FILE
fi


# initialize output file
RESULT_FILE="$WORKDIR_OSM"/changes_file.pbf
if [ -f "$RESULT_FILE" ]; then
  rm $RESULT_FILE
fi

#
# ovo se pokrece samo prvi put da se inicijalizuje radni direktorijum
# mkdir $WORKDIR_OSM
# osmosis --read-replication-interval-init workingDirectory=$WORKDIR_OSM
# otoriti https://replicate-sequences.osm.mazdermind.de/ kreirati state-file sadrzaj i snimiti ga u state.txt
# 

echo
echo "Downoading update to $DOWNLOAD_FILE" 
echo
osmosis --read-replication-interval workingDirectory=$WORKDIR_OSM --simplify-change --write-xml-change $DOWNLOAD_FILE 

# 
# Sada u $DOWNLOAD_FILE imamo sve izmene koje su nastale za celu planetu.
# Izgleda da nema nacina da se preprocesiranjem iyvade smao podaci koji su u okviru bbox, vec da bi to moralo u bazi da se radi
# 

# 
# ovde bi trebalo uraditi obradu preslovljavanja
# 


# ucitava pripremu baze
if $SEED_DATABASE; then
  cd $HOME_DIR
  #. $HOME_DIR/priprema_baze.sh
  
  # dodajemo u bazu 
  #seed_file_to_database "$RESULT_FILE" true 
  
fi


echo '================================================='
echo 'Zapoceto: '$m_start_timer

m_end_timer=$(date)
echo 'Zavrseno:' $m_end_timer

D1=$(date -d "$m_start_timer" '+%s'); D2=$(date -d "$m_end_timer" '+%s')
echo "Trajanje: $(((D2-D1)/86400))d $(date -u -d@$((D2-D1)) +%H:%M)"
echo '================================================='

echo
