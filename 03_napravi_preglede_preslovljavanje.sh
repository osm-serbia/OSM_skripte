#!/bin/sh

for file in $( ls -1 Regioni/*.osm )
do
    echo "java -cp serbiantransliterator.jar PregledNaziva -ulaz=$file "
    java -Dfile.encoding=UTF-8 -cp serbiantransliterator.jar PregledNaziva -ulaz=$file 
done

rm zaSajt/RS*.html
mv RS*.html zaSajt/
