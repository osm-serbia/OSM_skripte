#!/bin/sh

for file in $( ls -1 Regioni/*.osm )
do
    echo "java -cp serbiantransliterator.jar OSMParser -ulaz=$file -lektura -azuriratiName -azuriratiNameSr -azuriratiNameSrLatn"
    java -Dfile.encoding=UTF-8 -cp serbiantransliterator.jar OSMParser -ulaz=$file -lektura -azuriratiName -azuriratiNameSr -azuriratiNameSrLatn
done

rm zaSajt/lektura*
# premesta fajlove u direktoriju zaLekturu
mv lektura_RS* zaSajt/

# ne secam zasto se generisao ovaj fajl
rm rezultat.osm
