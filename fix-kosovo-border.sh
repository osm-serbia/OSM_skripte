#!/bin/sh

# Ova skripta je dodatak minutely mapnik-u gde je treba dodati u izvrsavanje "openstreetmap-tiles-update-expire" skripte.
# Skripta je idempotentna (moze se izvrsavati kad god i koliko god).
# Najbolje ju je dodati posle uvoza podataka, a pre invalidiranja tile-ova (dakle posle zvanja osm2ogsql, a pre render_expired)
# Evo ga primer kako se moze pozvati:
#
# m_ok "Fixing Kosovo border"
# ./fix_kosovo_border.sh >> "$RUNLOG"
#

set -e

# prvo pokupimo id-ove svih granica koje nam trebaju
kosovo_id=`psql -X -A -d gis -t -c "select distinct osm_id from planet_osm_roads where admin_level='2' and boundary='administrative' and tags -> 'name:sr' = 'Косово' limit 1"`
serbia_id=`psql -X -A -d gis -t -c "select distinct osm_id from planet_osm_roads where admin_level='2' and boundary='administrative' and tags -> 'name:sr' = 'Србија' limit 1"`
serbia_claimed_id=`psql -X -A -d gis -t -c "select distinct osm_id from planet_osm_roads where boundary='claimed_administrative' and tags -> 'name:sr' = 'Србија' and tags -> 'claimed:admin_level' = '2' limit 1"`

echo "Nađena granica Kosova je $kosovo_id, granica Srbije je $serbia_id, a claimed granica je $serbia_claimed_id"

if [ -n "$kosovo_id" ];
then
        echo "Granica sa Kosovom postoji, prepravljam je"
        # drop admin levels from Kosovo
        psql -X -A -d gis -t -c "delete from planet_osm_roads where osm_id<0 and (admin_level='6' or admin_level='4') and boundary='administrative' and way@(select way from planet_osm_polygon where osm_id=$kosovo_id)"
        # change kosovo border
        psql -X -A -d gis -t -c "update planet_osm_roads set admin_level=4 where osm_id=$kosovo_id"
        psql -X -A -d gis -t -c "update planet_osm_line set admin_level=4 where osm_id=$kosovo_id"
        psql -X -A -d gis -t -c "update planet_osm_polygon set admin_level=4 where osm_id=$kosovo_id"
        # rename Kosovo
        psql -X -A -d gis -t -c "update planet_osm_roads set name='Косово и Метохија', tags = tags || '\"name:sr-Latn\"=>\"Kosovo i Metohija\" ' where osm_id=$kosovo_id"
        psql -X -A -d gis -t -c "update planet_osm_polygon set name='Косово и Метохија', tags = tags || '\"name:sr-Latn\"=>\"Kosovo i Metohija\" ' where osm_id=$kosovo_id"
fi

if [ -n "$serbia_id" ];
then
        if [ -n "$serbia_claimed_id" ];
        then
                echo "Postoje i claimed i stara granica Srbije, brisem staru"
                # delete Serbia border
                psql -X -A -d gis -t -c "delete from planet_osm_roads where osm_id=$serbia_id"
                psql -X -A -d gis -t -c "delete from planet_osm_polygon where osm_id=$serbia_id"
        fi
fi

if [ -n "$serbia_claimed_id" ];
then
        echo "Claimed granice Srbije postoji, treba da je prepravimo"
        # set claimed border as real border
        psql -X -A -d gis -t -c "update planet_osm_roads set admin_level=2, boundary='administrative' where osm_id=$serbia_claimed_id"
        psql -X -A -d gis -t -c "update planet_osm_polygon set admin_level=2, boundary='administrative' where osm_id=$serbia_claimed_id"
fi
