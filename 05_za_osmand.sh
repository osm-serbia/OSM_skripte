#!/bin/sh

cd /home/renderaccount/OSM_skripte/zaOsmAnd/

mkdir -p existingIndexes
mkdir -p generation
mkdir -p index_files
mkdir -p osm_files
mkdir -p srtm

#srtm fajlovi:
#http://builder.osmand.net/terrain-aster-srtm-eudem/
#https://rutracker.org/forum/viewtopic.php?t=5393970 

#rm -f serbia-latest.osm.pbf
#wget https://download.geofabrik.de/europe/serbia-latest.osm.pbf

#echo ------ Raspakuje fajl u serbia.osm ------
# i uklanja uslovna ogranicenje brzine kada je sneg
osmosis --rb serbia-latest.osm.pbf --wx - | grep -vE 'maxspeed:conditional.*snow' > /home/renderaccount/OSM_skripte/zaOsmAnd/osm_files/srbija.osm

#preslovljavanje
#java -cp serbiantransliterator.jar OSMParser -ulaz=serbia.osm -name=latinica -azuriratiName -izlaz=zaOsmand/serbia_lat.osm

#rm serbia.osm

cd /home/renderaccount/src/osmAndMapCreator/

java -Djava.util.logging.config.file=logging.properties -Xms64M -Xmx1500M -cp "./OsmAndMapCreator.jar:lib/OsmAnd-core.jar:./lib/*.jar" net.osmand.util.IndexBatchCreator ../../OSM_skripte/zaOsmAnd/batch.xml
