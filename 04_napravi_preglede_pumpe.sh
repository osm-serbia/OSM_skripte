#!/bin/sh

rm zaPumpe/*

for file in $( ls -1 Regioni/*.osm )
do
    echo "java -cp serbiantransliterator.jar OSMPregledPumpi -ulaz=$file "
    java -cp serbiantransliterator.jar OSMPregledPumpi -ulaz=$file 
    echo "java -cp serbiantransliterator.jar OSMPregledPumpi -ulaz=$file -lpg"
    java -cp serbiantransliterator.jar OSMPregledPumpi -ulaz=$file -lpg
done     

# fajl srbia.osm koji je u radnom direktorijum
java -cp serbiantransliterator.jar OSMPregledPumpi -ulaz=serbia.osm -kml
java -cp serbiantransliterator.jar OSMPregledPumpi -ulaz=serbia.osm -lpg -kml

mv RS*.html zaPumpe/
mv RS*.kml zaPumpe/

mv serbia_pumpe* zaPumpe/