
MIN_ZOOM_TO_PRERENDER=7
MAX_ZOOM_TO_PRERENDER=11
MAX_ZOOM_TO_DEEP_PRERENDER=18
PRERENDER_BOUNDING_BOX="-x 18.7 -X 23 -y 42 -Y 46.3"
RENDERING_THREADS=1
RENDERING_SYSTEM_LOAD=16
FORCE_RENDERING_TILES=true
#RENDERING_SYSTEM_LOAD=8

echo
echo "PRERENDERING"
echo
echo "MIN_ZOOM_TO_PRERENDER: $MIN_ZOOM_TO_PRERENDER"
echo "MAX_ZOOM_TO_PRERENDER: $MAX_ZOOM_TO_PRERENDER"
echo "MAX_ZOOM_TO_DEEP_PRERENDER: $MAX_ZOOM_TO_DEEP_PRERENDER"
echo "PRERENDER_BOUNDING_BOX: $PRERENDER_BOUNDING_BOX"
echo "RENDERING_THREADS: $RENDERING_THREADS"
echo "RENDERING_SYSTEM_LOAD: $RENDERING_SYSTEM_LOAD"
echo "FORCE_RENDERING_TILES: $FORCE_RENDERING_TILES"
echo

m_start_timer=$(date)
echo 'Zapoceto: '$m_start_timer
echo

# Radni direktorijum skripte
HOME_DIR="$( cd "$(dirname "$0")" ; pwd -P )"


MIN_ZOOM_TO_PRERENDER_DEEP=$(($MAX_ZOOM_TO_PRERENDER+1))

if [ $FORCE_RENDERING_TILES = true ]; then
  fcmd="-f"
else
  fcmd=""
fi

command=$1

if [ -z "$command" ]; then
  command=""
fi

if [ "$command" = "deep" ]; then    
  echo 
  echo "Deep prerendering zoom"
  echo
  z=$MIN_ZOOM_TO_PRERENDER_DEEP
  Z=$MAX_ZOOM_TO_DEEP_PRERENDER
#  ~/src/render_list_geo.pl/render_list_geo.pl $fcmd -m ajt -n $RENDERING_THREADS -l $RENDERING_SYSTEM_LOAD -z $z -Z $Z $PRERENDER_BOUNDING_BOX
#  ~/src/render_list_geo.pl/render_list_geo.pl $fcmd -m lat -n $RENDERING_THREADS -l $RENDERING_SYSTEM_LOAD -z $z -Z $Z $PRERENDER_BOUNDING_BOX

else
  echo 
  echo "Standard prerendering zoom"
  echo
  z=$MIN_ZOOM_TO_PRERENDER
  Z=$MAX_ZOOM_TO_PRERENDER
  
#  ~/src/render_list_geo.pl/render_list_geo.pl $fcmd -m ajt -n $RENDERING_THREADS -l $RENDERING_SYSTEM_LOAD -z $z -Z $Z $PRERENDER_BOUNDING_BOX
#  ~/src/render_list_geo.pl/render_list_geo.pl $fcmd -m lat -n $RENDERING_THREADS -l $RENDERING_SYSTEM_LOAD -z $z -Z $Z $PRERENDER_BOUNDING_BOX
fi

cmd="$HOME_DIR/../../src/render_list_geo.pl/render_list_geo.pl $fcmd -m ajt -n $RENDERING_THREADS -l $RENDERING_SYSTEM_LOAD -z $z -Z $Z $PRERENDER_BOUNDING_BOX"
echo $cmd 
echo
$cmd

cmd="$HOME_DIR/../../src/render_list_geo.pl/render_list_geo.pl $fcmd -m lat -n $RENDERING_THREADS -l $RENDERING_SYSTEM_LOAD -z $z -Z $Z $PRERENDER_BOUNDING_BOX"
echo $cmd 
echo
$cmd

echo '================================================='
echo 'Zapoceto: '$m_start_timer

m_end_timer=$(date)
echo 'Zavrseno:' $m_end_timer

D1=$(date -d "$m_start_timer" '+%s'); D2=$(date -d "$m_end_timer" '+%s')
echo "Trajanje: $(((D2-D1)/86400))d $(date -u -d@$((D2-D1)) +%H:%M)"
echo '================================================='
echo
echo
