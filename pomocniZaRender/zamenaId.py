# -*- coding: utf-8 -*-
import codecs
izlaz = codecs.open('prvaIteracija.osm','w','utf-8')

brojac = 1000
dict = {}
idBroj = ""

with codecs.open("granice.osm",'r',encoding='utf8') as f:
    for line in f:
        start = line.find('id=\'')
#        print line

        if (start>0):
            end = line.find('\'', start + 6)
            idBroj = line[start+4:end]
            if(not idBroj in dict):
                brojac = brojac + 1
                dict[idBroj]=brojac
            print brojac, idBroj

           # print idBroj, dict[idBroj]
            izlaz.write(line.replace(idBroj, str(dict[idBroj])))
        else:
            izlaz.write(line)

izlaz.close()



izlaz = codecs.open('drugaIteracija.osm','w','utf-8')

with codecs.open("prvaIteracija.osm",'r',encoding='utf8') as f:
    for line in f:
        start = line.find('ref=\'')
#        print line

        if (start>0):
            end = line.find('\'', start + 6)
            idBroj = line[start+5:end]
            if idBroj in dict:
                izlaz.write(line.replace(idBroj, str(dict[idBroj])))
            else:
                izlaz.write(line)

        else:
            izlaz.write(line)

izlaz.close()
