import os
import sys
import urllib.request
import csv
import math
import collections
import datetime

from urllib.error import URLError, HTTPError



osnovnaPutanja = "/var/www/html/kes/"
graniceZaKes = "serbia.poly"
samoNedostajuce = True

home = os.path.expanduser("~")


##########################################################


#Preuzima i snima kes tile u lokalni direktorijum
def getTile(zoom, x, y):
    putanjaFajla = osnovnaPutanja+str(zoom)+"/"+str(x)+"/"+str(y)+".png"
    if os.path.exists(putanjaFajla):
        print("   Vec postoji")
        return

    dir = os.path.dirname(osnovnaPutanja+str(zoom)+"/"+str(x)+"/")
    if not os.path.exists(dir):
        os.makedirs(dir)

    try:
        urllib.request.urlretrieve("http://localhost/hot/"+str(zoom)+"/"+str(x)+"/"+str(y)+".png", putanjaFajla)
    except URLError as e:
        print(str(e))
        log(str(e))

# daje brojeve tile-a za odredjene koordinate
def deg2num(lat_deg, lon_deg, zoom):
  lat_rad = math.radians(lat_deg)
  n = 2.0 ** zoom
  xtile = int((lon_deg + 180.0) / 360.0 * n)
  ytile = int((1.0 - math.log(math.tan(lat_rad) + (1 / math.cos(lat_rad))) / math.pi) / 2.0 * n)
  return (xtile, ytile)


def izracunajPotrebneTileove(zoom):
    global dict
    dict = {}
    dictTacaka = {}
    poslednjiX = -1
    with open(graniceZaKes) as csvfile:
        readCSV = csv.reader(csvfile, delimiter=' ')
        for row in readCSV:
            if len(row) > 5:
                x, y = deg2num(float(row[6]), float(row[3]), zoom)
                dictTacaka[x] = y
                dodajTacku(x,y)
                #print(x, y)
                if poslednjiX > 0:
                    if poslednjiX < x:
                        smer = 1
                    else:
                        smer = -1
                    for i in range (poslednjiX, x, smer):
                        y = int(round(dictTacaka[poslednjiX] + (dictTacaka[x]-dictTacaka[poslednjiX])*(i-poslednjiX)/(x-poslednjiX)))
                        #print(" + ", i, y)
                        dictTacaka[i] = y
                        dodajTacku(i,y)


                poslednjiX = x

    print("izracunajPotrebneTilove", zoom, dict)
    return collections.OrderedDict(sorted(dict.items()))

def dodajTacku(x,y):
    global dict
    if x in dict:
        if y < dict[x][0]:
            dict[x] = [y, dict[x][1]]
        if y > dict[x][1]:
            dict[x] = [dict[x][0], y]
    else:
        dict[x] = [y,y]


def uzmiLiniju(zoom, x, y1, y2):
    for y in range(y1, y2 + 1):
        print("* uzmiLiniju", zoom, x, y)
        getTile(zoom, x, y)


def log(tekst):
    text_file = open(home+"/OSM_skripte/logs/kreirajKes.log", "a")
    text_file.write(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")+ " " + tekst + "\n")
    text_file.close()

def main(minZoom, maxZoom):
	pocetak = datetime.datetime.now()

	for zoom in range(minZoom, maxZoom + 1):
	    log("Pocetak zuma: "+graniceZaKes+" "+str(zoom))
	    pocetakZoom = datetime.datetime.now()

	    sortiranDict = izracunajPotrebneTileove(zoom)

	    for k in sortiranDict:
	        print(k, sortiranDict[k][0], sortiranDict[k][1])
	        uzmiLiniju(zoom, k, sortiranDict[k][0], sortiranDict[k][1])

	    log("Kraj zuma: " + graniceZaKes + " " + str(zoom) + " " + str(datetime.datetime.now()-pocetakZoom) )


	print(pocetak)
	kraj = datetime.datetime.now()
	print(kraj)
	print(kraj-pocetak)

	print("Gotovo.")


if __name__ == "__main__":
#    global graniceZaKes
    graniceZaKes = sys.argv[3]
    main(int(sys.argv[1]), int(sys.argv[2]))
