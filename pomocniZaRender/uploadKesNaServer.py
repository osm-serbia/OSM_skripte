import os
import requests
import hashlib
from functools import partial

putanjaDoKesa = "/var/www/html/kes/"
adresaZaUpload='http://mpele.000webhostapp.com/uploadKes.php'
#adresaZaUpload= "http://osm2.mpele.in.rs/uploadKes.php"
adresaZaMD5='http://mpele.000webhostapp.com/getMD5.php'
#adresaZaMD5='http://osm2.mpele.in.rs/getMD5.php'



def posaljiFajl(z, x, y):
    files = {'fileToUpload': open(putanjaDoKesa+str(z)+'/'+str(x)+'/'+y, 'rb')}

    payload = {
    'z': z,
    'x': x,
    'y': y
    }

    res = requests.post(
        url=adresaZaUpload,
        files=files, data=payload)

    print(res.text)


def md5sum(filename):
    with open(filename, mode='rb') as f:
        d = hashlib.md5()
        for buf in iter(partial(f.read, 128), b''):
            d.update(buf)
    return d.hexdigest()


def getMD5(z, x):
    payload = {
    'z': z,
    'x': x
    }

    res = requests.post(
        url=adresaZaMD5,
        data=payload)

    return res.text


def getMD5_saServera(zoom,x):
    md5NaServeru = getMD5(zoom,x)
    md5Dict = {}
    for line in md5NaServeru.splitlines():
        sline = line.split(" ")
        if(len(sline) > 1):
            #print(sline[0], sline[1])
            md5Dict[sline[0]] = sline[1]
    return md5Dict


def obradiDirektorijum(zoom, x):
    md5DictSaHostingServera = getMD5_saServera(zoom ,x)
    fajlovi = os.listdir(putanjaDoKesa+str(zoom)+'/'+str(x)+'/')

    for fajl in fajlovi:
        md5Local =  md5sum(putanjaDoKesa+str(zoom)+'/'+str(x)+'/'+fajl)

        if fajl in md5DictSaHostingServera and md5DictSaHostingServera[fajl] == md5Local:
            print(fajl, md5DictSaHostingServera[fajl], md5Local, md5DictSaHostingServera[fajl] == md5Local)
        else:
            print(fajl, 'Salje')

            posaljiFajl(zoom, x, fajl)

def rekurzivnoObradiPostojeceDirektorijume():
    filenames = os.listdir (putanjaDoKesa)

    result = []
    for filename in filenames: # loop through all the files and folders
        if os.path.isdir(os.path.join(os.path.abspath(putanjaDoKesa), filename)): # check whether the current object is a folder or not
            direktorijumi = os.listdir (putanjaDoKesa+filename)
            for dir in direktorijumi:
                print("Obradjuje ", filename, dir)
                obradiDirektorijum(filename, dir)



###################################################################
if __name__ == "__main__":
     rekurzivnoObradiPostojeceDirektorijume()
#    obradiDirektorijum(9, 286)




