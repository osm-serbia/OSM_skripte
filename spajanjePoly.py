import alphashape
#import matplotlib.pyplot as plt
from descartes import PolygonPatch
import os

points_2d = []

def ucitajTacke(nazivFajla):
   file1 = open(nazivFajla, 'r')
   Lines = file1.readlines()
 
   count = 0
   # Strips the newline character
   for line in Lines:
      linija = line.split()
      if len(linija)==2:
           points_2d.append( (float(linija[0]), float(linija[1])) )


for file in os.listdir("."): # definisati putanju do fajlova !!!! ucitava i izlaz.poly
    if file.endswith(".poly"):
        print(os.path.join("ucitava tacke iz: ", file))
        ucitajTacke(file)


#ucitajTacke('serbia.poly')
#ucitajTacke('kosovo.poly')
#ucitajTacke('montenegro.poly')
#ucitajTacke('bosnia-herzegovina.poly')
#ucitajTacke('croatia.poly')


alpha_shape = alphashape.alphashape(points_2d, 0.)
alpha_shape

#fig, ax = plt.subplots()
#ax.scatter(*zip(*points_2d))
#ax.add_patch(PolygonPatch(alpha_shape, alpha=0.2))
#plt.show()

#print(alpha_shape.exterior.coords.xy)

x,y = alpha_shape.exterior.coords.xy
#print('x ', x[2])


fileOut = open("izlaz.poly", "w")
fileOut.writelines('none\n1\n')
for i in range(0, len(x)):
    fileOut.writelines('   ' + str("{0:.2E}".format(x[i])) + '   ' + str(str("{0:.2E}".format(y[i])) + '\n'))
fileOut.writelines('END\nEND\n')
fileOut.close()
