#!/bin/sh

# obradjuje podatke za jednu zemlju
# $1 - identifikator zemlje (serbia, montenegro...)
# $2 - sablon preslovljavanja u cirilicu
# $3 - sablon preslovljavanja u latinicu

process_country () {

  local country=$1
  local tslt_cir=$2
  local tslt_lat=$3
  local output_file=$4
  
  echo
  echo ----------
  echo $(date)
  echo "Obrada zemlje: $1"
  echo
  
  local file_osm=$(get_country_file_name "$country" "osm")
  
  local file_osm_exists=0
  if [ -f "$file_osm" ];
  then
    file_osm_exists=1
  fi
 

#echo "file_osm " $file_osm
echo "USE_LOCAL_DATA" $USE_LOCAL_DATA
#echo "file_osm_exists " $file_osm_exists

  if [ $USE_LOCAL_DATA = true ] && [ $file_osm_exists -eq 1 ];
  then
    echo "Vec postoji pripremljeno: "$file_osm
  else

    get_country_file $country osm
    file_osm=$get_country_file_result
    
    tmp_file=$(get_temp_file_name "$file_osm")

    transliterate $file_osm $tslt_cir $tslt_lat $tmp_file
 
    mv $tmp_file $file_osm
 
  fi
  
  file_list="$file_list $file_osm"  

  [ ! -z "$output_file" ] && output_file_set=true || output_file_set=false

  # ako je inkrementalno prepisivanje onda se odmah prepisuju podaci
  # takodje, ako je naveden output_file onda nije bitno da li je podeseno 
  # inkrementalno, svakako se prepisuje
  if [ "$INCREMENTAL_PROCESS" = true ] || [ "$output_file_set" = true ];  then  

    if [ -z "$output_file" ]
    then
      output_file=$RESULT_FILE
    fi
    
    merge_files_add $output_file $file_osm    
  fi
  
  echo
  echo "Zavrsena obrada zemlje: $1 u $file_osm"
  echo
  
}

# preuzima datoteku sa urla
# $1 - url sa kog se preuzima datoteka
# $2 - putanja i naziv datoteke u koju se snima
#
get_file () {
  local m_url=$1
  local m_destination=$2
  local m_save_dir=$(pwd)

  echo  
  echo "Downloading from:" $m_url " to " $m_destination
  cd $TMP_DIR
set -x
  wget -O $m_destination $m_url
set +x
  cd $m_save_dir
  echo

}

# vraca naziv datoteke za navedenu drzavu
#
# $1 - identifikator drzave
# $2 - identifikator formata izlazne datoteke (pbf, osm)
#
# vraca putanju do datoteke kao globalnu promenljivu $get_country_file_result
#
get_country_file_name () {

  local m_country=$1
  local m_file_format=$2

  local m_convert_to_osm=0
  
  if [ "$m_file_format" = "osm" ]
  then
    m_convert_to_osm=1
  fi

#echo "LOCAL_DATA_DIR: $LOCAL_DATA_DIR"

  local m_local_pbf_file=$LOCAL_DATA_DIR/$m_country-latest.osm.local.pbf

#echo "m_local_pbf_file: $m_local_pbf_file"

  local m_result_file=""
  
  if [ "$m_convert_to_osm" = 1 ]
  then
    m_result_file=$m_local_pbf_file.osm
  else
    m_result_file=$m_local_pbf_file
  fi  
  
  echo $m_result_file

}

# preuzima bazu za navedenu drzavu
#
# $1 - identifikator drzave
# $2 - identifikator formata izlazne datoteke (pbf, osm)
#
# vraca putanju do datoteke kao globalnu promenljivu $get_country_file_result
#
get_country_file () {

  local m_country=$1
  local m_file_format=$2

  m_convert_to_osm=0
  
  if [ "$m_file_format" = "osm" ]
  then
    m_convert_to_osm=1
  fi
  
  echo
  echo ----------
  echo $(date)
  echo "Getting data for:" $m_country
  
  #if [ "$m_convert_to_osm" = 1 ]
  #then
  #  echo " returning osm"
  #else
  #  echo " returning pbf"    
  #fi
  #echo
  
  local m_url=https://download.geofabrik.de/europe/$m_country-latest.osm.pbf
  #local m_local_pbf_file=$LOCAL_DATA_DIR/$m_country-latest.osm.local.pbf
  local m_local_pbf_file=$(get_country_file_name $country)
  
  
  #local m_result_file=""
  
  #if [ "$m_convert_to_osm" = 1 ]
  #then
  #  m_result_file=$m_local_pbf_file.osm
  #else
  #  m_result_file=$m_local_pbf_file
  #fi
  
  local m_result_file=$(get_country_file_name $country osm)
  
  # ovu granu uslova treba izbaciti. 
  # ova funkcija uvek treba da download-uje trazenu datoteku
  #if $USE_LOCAL_DATA 
  if [ 0 -eq 1 ];
  then
    if [ ! -f "$m_result_file" ]; then
      echo "Local data does not exist. Downloading $m_url to $m_local_pbf_file"
      get_file $m_url $m_local_pbf_file
      
      if [ "$m_convert_to_osm" = 1 ]
      then 
        echo "Converting to $m_result_file"
        convert_file $m_local_pbf_file $m_result_file
        rm $m_local_pbf_file
      fi
    else
      echo "Local data already exists. Using $m_result_file"
    fi
  else
    echo "Downloading $m_url to $m_local_pbf_file"
    get_file $m_url $m_local_pbf_file

    if [ "$m_convert_to_osm" = 1 ]
    then 
      echo "Converting to $m_result_file"
      convert_file $m_local_pbf_file $m_result_file
      rm $m_local_pbf_file
    fi
    
  fi
  
  get_country_file_result=$m_result_file
 
}

#
# vrati ime datoteke iz cele putanje
# $1 - putanja do datoteke
#
get_file_name () {
  local m_file_name=$(basename -- "$1")
  get_file_name_result="${m_file_name%.*}"
  echo $get_file_name_result
}

#
# vrati ekstenziju iz cele putanje, ono posle zadnje tacke
# $1 - putanja do datoteke
#
get_file_ext () {
  local m_file_name=$(basename -- "$1")
  get_file_ext_result="${m_file_name##*.}"
  echo $get_file_ext_result
}

#
# convert file $1 to file $2
# just passes call to merge_files()
#
convert_file () {
 merge_files $2 $1
}


#
# create temporary file name based on provided file name
#
get_temp_file_name () {
  local m_output_file=$1
  
  local m_file_name=$TMP_DIR/$(get_file_name "$m_output_file")
  
  local m_ext=$(get_file_ext $m_output_file)
  
  local m_temp_file=$(mktemp -u "$m_file_name".XXXXXXXXX."$m_ext")
  
  echo $m_temp_file

}


#
# Adding to destination file merged files using osmosis
# Based on merge_files ()
#
# $1 - file to write output to
# $2++ - files to merge
#
# If output file exists, merged data will be added to that file
#
# It works with all file formats osmosis can do. File format is decided by extension (.pbf, .osm)
# If just two files are specified converts second file ($2) to first ($1). 
# If more files are specified al files ($2, $3, $4...) are merged to first file name ($1).
#

merge_files_add () {

  echo "Adding merge: " "$@"

  local m_output_file=$1
  
  get_file_ext $m_output_file
  local m_ext=$get_file_ext_result
  
  #local m_temp_file=$(mktemp "$m_output_file".XXXXXXXXX."$m_ext")
  m_temp_file=$(get_temp_file_name "$m_output_file")
 
  if [ -f "$m_temp_file" ]
  then
    rm $m_temp_file
  fi
  
  if [ -f "$m_output_file" ]
  then
    mv $m_output_file $m_temp_file
    merge_files "$@" $m_temp_file
    rm $m_temp_file
  else
    merge_files "$@"
  fi
  
}


#
# Merge files using osmosis
#
# $1 - file to write output to
# $2++ - files to merge
#
# It works with all file formats osmosis can do. File format is decided by extension (.pbf, .osm)
# If just two files are specified converts second file ($2) to first ($1). 
# If more files are specified al files ($2, $3, $4...) are merged to first file name ($1).
#
merge_files () {

  echo 
 
  local OSM_EXT="osm"
  
  local m_output=$1
 
  local m_output_ext=$(get_file_ext $m_output)
  #local m_output_ext=$get_file_ext_result
 
  local m_output_cmd=""
  if [ "$m_output_ext" = "$OSM_EXT" ]
  then
    m_output_ext="xml"
    m_output_cmd="--write-"$m_output_ext
  else
    m_output_cmd="--write-$m_output_ext omitmetadata=true"
  fi
 
  local m_input_cmd_list=""
  
  local m_input_count=0
  
  shift 
  
  local m_input=""

  local m_merge_cmd=""
  
  for m_input in "$@"
    do
    
    m_input_count=$((m_input_count + 1))
    
    echo "Input: $m_input"

    local m_input_ext=$(get_file_ext $m_input)
    #local m_input_ext=$get_file_ext_result
    
    if [ "$m_input_ext" = "$OSM_EXT" ]
    then
      m_input_ext="xml"
    fi
    
    local m_input_cmd="--read-"$m_input_ext
    
    local m_input_cmd_list="$m_input_cmd_list $m_input_cmd $m_input"

    if [ "$m_input_count" -gt 1 ]
    then
      m_merge_cmd="$m_merge_cmd --merge "
    fi
    
  done 
  
  
  
  #if [ "$m_input_count" -gt 1 ]
  #then
  #  m_merge_cmd="--merge"
  #fi
  
  echo "Output:" $m_output
  echo
 
set -x  
  osmosis $m_input_cmd_list $m_merge_cmd --bounding-box $BOUNDING_BOX $m_output_cmd $m_output
set +x    
  
#  if !$USE_LOCAL_DATA 
#  then
#    for m_input in "$@"
#    do
#      if [ -f "$m_input" ]
#      then
#        rm $m_input
#      fi
#    done   
#  fi
 
  echo

}



#
# obrise navedene tagove pomocu osmfilter
# $1 - ulazna datoteka
# $2 - ozlazna datoteka
# $3,$4... - lista tagova koji se brisu 
#

filter_drop_tags () {
#echo "params:"  "$@"
#echo "a1"
  local m_input_file=$1
#echo "a2"
  local m_output_file=$2
#echo "a3"  
  
  shift
  shift
  
  local m_cmd=""
  
  local m_input=""
  
#echo "params:"  "$@"
  

  for m_input in "$@"
    do
    
    m_input_count=$((m_input_count + 1))
    
    echo "Tag: $m_input"

    local m_cmd="$m_cmd --drop-tags=\"$m_input\""
    
  done   
  
#echo m_cmd: $m_cmd
  
  #m_temp_file=$(get_temp_file_name "$m_output_file")
  cd "$TMP_DIR"
set -x
  osmfilter "$m_input_file" "$m_cmd" -o="$m_output_file"
set +x
  cd "$HOME_DIR"
  #mv $m_temp_file $m_output_file
 
 
}


transliterate () {
  local m_file=$1
  local m_name_rules=$2
  local m_name_latn_rules=$3
  local m_new_filename=$4 #optional

  echo
  echo Transliterating $m_file



  if [ -z $m_new_filename ]
  then
    local m_temp_file=$(get_temp_file_name "$m_file")
    mv $m_file $m_temp_file
  else
    local m_temp_file=$m_file
    m_file=$m_new_filename
  fi

set -x  
  cd "$HOME_DIR"
  java -cp serbiantransliterator.jar OSMParser_Name_NameSrLatn -ulaz="$m_temp_file" -izlaz="$m_file" $m_name_rules $m_name_latn_rules
set +x  

  if [ -z $m_new_filename ]
  then
    rm $m_temp_file
  fi

  cd $TMP_DIR
  echo
  
}

replace_in_file () {
  
  local m_file=$1
  local m_search=$2
  local m_replacement=$3
  
  echo Replacing $m_search with $m_replacement in $m_file
  
  local m_temp_file=$(get_temp_file_name "$m_file")
  
  mv $m_file $m_temp_file
set -x  
  sed "s/$m_search/$m_replacement/g" $m_temp_file > $m_file
set +x  
  rm $m_temp_file

}


seed_database () {

  local m_input=""
  local m_is_append=false
  
  for m_input in "$@"
    do

      m_input="${m_input#"${m_input%%[![:space:]]*}"}"
      
      if [ -f "$m_input" ]; then
        echo "Prepisivanje u bazu: ""$m_input"
        seed_file_to_database "$m_input" "$m_is_append"
      else
        echo 
        echo "Ne postoji: [$m_input]"
        echo
        exit
      fi
      m_is_append=true
  done    

}

seed_file_to_database () {
  
  local m_file=$1
  
  
  local command
  local to_append=$2

  #[ -z "$to_append" ] && to_append=false || to_append=$to_append
  
  if $to_append; then
    command="-a"
  else
    command="-c"
  fi

  echo 
  echo "Statistics for $m_file"
  echo
  osmium fileinfo -e $m_file
  echo

  echo
  echo ----------
  echo $(date)
  echo "Ucitavanje podataka u bazu: $m_file"
  echo
  set -x
  /usr/local/bin/osm2pgsql $command --slim -d gis -C "$memZaOsm2pgsql" --hstore -S /home/renderaccount/src/openstreetmap-carto/openstreetmap-carto.style --tag-transform-script /home/renderaccount/src/openstreetmap-carto/openstreetmap-carto.lua "$m_file"
  set +x
  #osm2pgsql -c --slim -d gis -C 700 --hstore -S /home/renderaccount/src/openstreetmap-carto/openstreetmap-carto.style merged_sr_ko_cg_al_mk.pbf
  #osm2pgsql -c --slim -d gis -C 2700 --hstore -S /home/renderaccount/src/openstreetmap-carto/openstreetmap-carto.style merged_sr_ko_cg_al_mk.pbf
  #rm -f "$m_file"
}


trim() {
    local var="$*"
    # remove leading whitespace characters
    var="${var#"${var%%[![:space:]]*}"}"
    # remove trailing whitespace characters
    var="${var%"${var##*[![:space:]]}"}"   
    echo -n "$var"
}
