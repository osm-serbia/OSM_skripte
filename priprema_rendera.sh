#!/bin/sh
set -e
#set -x

echo

# Radni direktorijum skripte
HOME_DIR="$( cd "$(dirname "$0")" ; pwd -P )"
cd $HOME_DIR

DEFAULT_CONFIG_FILE="$HOME_DIR/priprema_rendera_config.sh"
#DEFAULT_CONFIG_FILE="priprema_rendera_config.sh"

config_id=$1

if [ -z "$config_id" ]; then
  CONFIG_FILE="$DEFAULT_CONFIG_FILE"
else
  CONFIG_FILE="$HOME_DIR/priprema_rendera_config_$config_id.sh"
fi

#inicijalizacija
INIT_FILE="$HOME_DIR/priprema_rendera_init.sh"
. $INIT_FILE


#ucitava podesavanja
if [ -f "$CONFIG_FILE" ]
then
  echo 'Konfiguracija: '$CONFIG_FILE
  . $CONFIG_FILE
else
  echo
  echo 'Nije nadjena konfiguracija: '$CONFIG_FILE
  echo 'Prekopirajte priprema_rendera_config_primer.sh u priprema_rendera_config.sh'
  echo
  exit
fi

#ucitava podesavanja
#. $HOME_DIR/priprema_rendera_config.sh


#ucitava biblioteku
. $HOME_DIR/priprema_rendera_lib.sh

# podesavanje sta se obradjuje u priprema_rendera je prebaceno u config

echo ''
echo ''
echo ''
echo "====================================================="
echo 'Priprema podataka za render'
echo "-----------------------------------------------------"
echo ''

m_start_timer=$(date)
echo 'Zapoceto: '$m_start_timer
echo ''


# initialize output file

#RESULT_FILE="$TMP_DIR"/base_file.osm
RESULT_FILE="$TMP_DIR"/base_file.pbf
if [ -f "$RESULT_FILE" ]; then
  rm $RESULT_FILE
fi


file_list=""


if $PROCESS_SERBIA || $PROCESS_KIM;
then
# process serbia

  echo
  echo ----------
  echo $(date)
  echo "Obrada Srbije"
  echo

  #serbia_full_osm=$LOCAL_DATA_DIR/serbia_full.osm

  #if [ ! $USE_LOCAL_DATA ] || [ ! -f "$serbia_full_osm" ]; then
  
  #  if [ -f "$serbia_full_osm" ]; then
  #    rm $serbia_full_osm
  #  fi
    
    # Sredjivanje KiM
    if $PROCESS_KIM;
    then

      process_country kosovo $TSLT_KIM_CIR $TSLT_KIM_LAT $serbia_full_osm
      #process_country kosovo $TSLT_KIM_CIR $TSLT_KIM_LAT
    
    fi # obrada KiM

    # ovde ide obrada srbije
    if $PROCESS_SERBIA;
    then

      process_country serbia $TSLT_CIR_CIR $TSLT_SRB_LAT $serbia_full_osm
      #process_country serbia $TSLT_CIR_CIR $TSLT_SRB_LAT
    
    fi 
   
  #fi
  
  # stavi u izlaznu datoteku
  #merge_files_add $RESULT_FILE $serbia_full_osm
  #rm $serbia_full_osm
  #echo
    
fi 
# ovde zavrsava obrada Srbije


if $PROCESS_MONTENEGRO; then
  process_country montenegro $TSLT_LAT_CIR $TSLT_LAT_LAT
fi

if $PROCESS_MACEDONIA; then
  process_country macedonia $TSLT_CIR_CIR $TSLT_CIR_LAT
fi

if $PROCESS_BULGARIA; then
  process_country bulgaria $TSLT_CIR_CIR $TSLT_CIR_LAT
fi

if $PROCESS_ROMANIA; then
  process_country romania $TSLT_LAT_CIR $TSLT_LAT_LAT
fi

if $PROCESS_ALBANIA; then
  process_country albania $TSLT_LAT_CIR $TSLT_LAT_LAT
fi

if $PROCESS_BOSNIA_HERZEGOVINA; then
  process_country bosnia-herzegovina $TSLT_LAT_CIR $TSLT_LAT_LAT
fi

if $PROCESS_HUNGARY; then
  process_country hungary $TSLT_LAT_CIR $TSLT_LAT_LAT
fi

if $PROCESS_CROATIA; then
  process_country croatia $TSLT_LAT_CIR $TSLT_LAT_LAT
fi

if $PROCESS_SLOVENIA; then
  process_country slovenia $TSLT_LAT_CIR $TSLT_LAT_LAT
fi


if $PROCESS_AUSTRIA; then
  process_country austria $TSLT_LAT_CIR $TSLT_LAT_LAT
fi

if $PROCESS_ITALY; then
  process_country italy $TSLT_LAT_CIR $TSLT_LAT_LAT
fi


if $PROCESS_GREECE; then
  process_country greece $TSLT_LAT_CIR $TSLT_LAT_LAT
fi


# ako podaci nisu inkrementalno prepisani sad se to radi sve odjednom
if [ "$INCREMENTAL_PROCESS" = false ]; then  
  echo
  echo ----------
  echo $(date)
  
  if [ $MERGE_TYPE = "file" ]; then
  
    echo "Prepisivanje svih podataka odjednom u datoteku $RESULT_FILE"
    echo 
    merge_files $RESULT_FILE $file_list
    
    file_list=$RESULT_FILE
    
  fi
fi


# ucitava pripremu baze
if $SEED_DATABASE; then
  cd $HOME_DIR
  . $HOME_DIR/priprema_baze.sh
fi

# pokreni prerender tileova
#if $SEED_DATABASE && $PRERENDER_TILES;
if $PRERENDER_TILES;
then

  . $HOME_DIR/priprema_rendera_prerender.sh
fi



echo '================================================='
echo 'Zapoceto: '$m_start_timer

m_end_timer=$(date)
echo 'Zavrseno:' $m_end_timer

D1=$(date -d "$m_start_timer" '+%s'); D2=$(date -d "$m_end_timer" '+%s')
echo "Trajanje: $(((D2-D1)/86400))d $(date -u -d@$((D2-D1)) +%H:%M)"
echo '================================================='

echo
