#!/bin/sh
set -e
#set -x

echo

# Radni direktorijum skripte
HOME_DIR="$( cd "$(dirname "$0")" ; pwd -P )"
cd $HOME_DIR

CONFIG_FILE="$HOME_DIR/priprema_rendera_config.sh"

#ucitava podesavanja
if [ -f "$CONFIG_FILE" ]
then
  . $CONFIG_FILE
else
  echo
  echo 'Nije nadjena konfiguracija: '$CONFIG_FILE
  echo 'Prekopirajte priprema_rendera_config_primer.sh u priprema_rendera_config.sh'
  echo
  exit
fi


#ucitava biblioteku
. $HOME_DIR/priprema_rendera_lib.sh

  echo
  echo ----------
  echo $(date)
  echo "Prerendering tile-ova"
  echo 
  echo "Prerendering MaxZoom: "$MAX_ZOOM_TO_PRERENDER
  echo "Prerendering BoundingBox: "$PRERENDER_BOUNDING_BOX
  echo

  if $CLEAR_TILE_CACHE;
  then
    #. $HOME_DIR/pomocniZaRender/brisanjeKesa.sh

    # obrise kes
    #echo
    #echo "Brisanje kesa"
    #echo
    #echo $TILESERVER_CACHE
    #rm -rf $TILESERVER_CACHE
    #echo $RENDER_CACHE_CIR
    #rm -rf $RENDER_CACHE_CIR
    #echo $RENDER_CACHE_LAT
    #rm -rf $RENDER_CACHE_LAT
    echo

  fi

  ~/src/render_list_geo.pl/render_list_geo.pl -f -m ajt -n $RENDERING_THREADS -l $RENDERING_SYSTEM_LOAD -z 7 -Z $MAX_ZOOM_TO_PRERENDER $PRERENDER_BOUNDING_BOX
  ~/src/render_list_geo.pl/render_list_geo.pl -f -m lat -n $RENDERING_THREADS -l $RENDERING_SYSTEM_LOAD -z 7 -Z $MAX_ZOOM_TO_PRERENDER $PRERENDER_BOUNDING_BOX

echo
echo
